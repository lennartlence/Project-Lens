import needle from "needle";

class TorrentSearch {
    constructor() {
        this.tracker = null;
        this.rutrackerCookie = null;
    };

    createActions() {
        this.loginRutracker();
    };

    loginRutracker() {
        const postData = require('querystring').stringify({
            login_username: 'LennartLence',
            login_password: 'f20o5r7g10e15t6',
            login: 'Вход',
        })

        const options = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': postData.length
            }
        };

        needle('post', 'https://rutracker.org/forum/login.php', postData, options)
            .then(resp => {
                if (resp.statusCode.toString() === '302') {
                    this.rutrackerCookie = resp.headers['set-cookie'][1];
                    console.log('Rutracker Login Done!')
                };
            });
    };

    search(query, tracker) {
        const postData = require('querystring').stringify({
            nm: query,
            f: '-1',
            o: 10
        })

        return new Promise((resolve, reject) => {
            switch (tracker) {
                case 'Nnm':
                    this.tracker = 1;

                    needle('get', `https://nnmclub.to/forum/tracker.php?${postData}`)
                        .then(function (resp) {
                            const page = new DOMParser().parseFromString(resp.body, "text/html");

                            let result = [];

                            page.querySelectorAll('#search_form > table.forumline.tablesorter > tbody > tr').forEach(item => {
                                let torrent = {
                                    id: 'item' + Math.random().toString(36).substr(2, 9),
                                    title: item.childNodes[5].textContent,
                                    size: `${item.childNodes[11].textContent.split(' ')[1]} ${item.childNodes[11].textContent.split(' ')[2]}`,
                                    seeds: item.childNodes[13].textContent,
                                    link: 'https://nnmclub.to/forum/' + item.childNodes[5].childNodes[0].href.split('/')[9]
                                };

                                result.push(torrent);
                            });

                            resolve(result);
                        })
                        .catch(function (err) {
                            reject(err)
                        })
                    break;

                case 'Rutracker':
                    this.tracker = 2;

                    const options = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Content-Length': postData.length,
                            'Cookie': this.rutrackerCookie
                        }
                    };

                    needle('post', 'https://rutracker.org/forum/tracker.php', postData, options)
                        .then(resp => {
                            const page = new DOMParser().parseFromString(resp.body, "text/html");

                            let result = [];

                            const items = page.querySelectorAll('#tor-tbl > tbody > tr');

                            items.forEach(item => {
                                if (item.childNodes[13].childNodes[2].title.length !== 12 && item.childNodes[3].title.length !== 7) {
                                    let torrent = {
                                        id: 'item' + Math.random().toString(36).substr(2, 9),
                                        title: item.childNodes[7].childNodes[1].childNodes[1].textContent,
                                        size: `${item.childNodes[11].childNodes[3].textContent.split(' ')[0]}`,
                                        seeds: item.childNodes[13].childNodes[2].textContent,
                                        link: 'https://rutracker.org/forum/' + item.childNodes[7].childNodes[1].childNodes[1].href.split('/')[9]
                                    };

                                    result.push(torrent);
                                };
                            });

                            resolve(result);
                        })
                        .catch(function (err) {
                            reject(err);
                        });
                    break;

                default:
                    break;
            };
        });
    };

    getMagnet(link) {
        return new Promise((resolve, reject) => {
            switch (this.tracker) {
                case 1:
                    needle('get', link)
                        .then(resp => {
                            const page = new DOMParser().parseFromString(resp.body, "text/html");

                            resolve(page.querySelector('td.gensmall > a').href);
                        })
                        .catch(function (err) {
                            reject(err);
                        });
                    break;
                case 2:
                    const options = {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Content-Length': 0,
                            'Cookie': this.rutrackerCookie
                        }
                    };

                    needle('post', link, {}, options)
                        .then(resp => {
                            const page = new DOMParser().parseFromString(resp.body, "text/html");

                            resolve(page.querySelector('.magnet-link-16').href);
                        })
                        .catch(function (err) {
                            reject(err);
                        });
                    break;

                default:
                    break;
            };
        });
    };
};

const torrentSearch = new TorrentSearch();

torrentSearch.createActions();

export { torrentSearch };

