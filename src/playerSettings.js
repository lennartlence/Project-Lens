import { player } from "./player";

class PlayerSettings {
    constructor() {
        this.div = document.querySelector('#playerSettings');
    };

    createActions() {
        document.querySelector('#subtitles').addEventListener('focus', () => {
            document.querySelector(`#subtitles .trackList`).classList.add('active')
        });
        document.querySelector('#subtitles').addEventListener('blur', () => {
            document.querySelector(`#subtitles .trackList`).classList.remove('active')
        });

        document.querySelector('#audio').addEventListener('focus', () => {
            document.querySelector(`#audio .trackList`).classList.add('active')
        });
        document.querySelector('#audio').addEventListener('blur', () => {
            document.querySelector(`#audio .trackList`).classList.remove('active')
        });

        document.querySelector('#subtitles').addEventListener('click', (e) => {
            if (e.target.id !== 'subtitles' && player.playerStatus == 2) {
                player.renderEngine.engine.subtitles.track = parseInt(e.target.id)
                document.querySelector('#currentSubtitle span').innerHTML = player.renderEngine.engine.subtitles[parseInt(e.target.id)];
            };
        });
        document.querySelector('#audio').addEventListener('click', (e) => {
            if (e.target.id !== 'audio' && player.playerStatus == 2) {
                player.renderEngine.engine.audio.track = parseInt(e.target.id);
                document.querySelector('#currentAudio span').innerHTML = player.renderEngine.engine.audio[parseInt(e.target.id)];
            };
        });
    };
};

const playerSettings = new PlayerSettings();

playerSettings.createActions();

export { playerSettings };