class RoomMenu {
    constructor() {
        this.div = document.querySelector('#roomMenu');
        this.joinBtn = document.querySelector('#roomMenuJoin');
        this.createBtn = document.querySelector('#roomMenuCreate');
    };

    createActions() {
        this.joinBtn.addEventListener('mouseover', () => {
            this.joinBtn.style.width = '100%';
            this.createBtn.style.width = 0;
        });
        this.joinBtn.addEventListener('mouseout', () => {
            this.joinBtn.style.width = '27px';
            this.createBtn.style.width = '111px';
            document.querySelector('#roomMenuJoin input').blur();
        });

        this.createBtn.addEventListener('click', () => {
            this.createRoomRequest();
        });

        this.joinBtn.addEventListener('keydown', (e) => {
            if (e.keyCode == 13) {
                this.joinRoomRequest();
            };
        });

        document.querySelector('#roomMenuJoin svg').addEventListener('mousedown', () => {
            this.joinRoomRequest();
        });

        document.querySelector('#roomMenuJoin input').addEventListener('contextmenu', (e) => {
            e.preventDefault();
            document.execCommand('paste');
        });

        document.querySelector('#roomId').addEventListener('click', () => {
            const roomId = document.querySelector('#roomId span');

            if (roomId.textContent !== 'Copied!') {
                roomId.style.userSelect = 'text';

                const selection = window.getSelection();
                const range = document.createRange();
                range.selectNodeContents(roomId);
                selection.removeAllRanges();
                selection.addRange(range);

                try {
                    document.execCommand('copy');
                    selection.removeAllRanges();
                    roomId.style.userSelect = 'none';

                    const original = roomId.textContent;
                    roomId.textContent = 'Copied!';

                    setTimeout(() => {
                        roomId.textContent = original;
                    }, 1000);
                } catch (e) {
                    console.log('err')
                }
            };
        });
    };

    joinRoomRequest() {
        this.joinBtn.style.transform = 'scale(0.9)';
        setTimeout(() => {
            this.joinBtn.style.transform = 'scale(1)';
        }, 250);
    };

    createRoomRequest() {
        this.createBtn.style.transform = 'scale(0.9)';
        setTimeout(() => {
            this.createBtn.style.transform = 'scale(1)';
        }, 250);
    };
};

const roomMenu = new RoomMenu();

roomMenu.createActions();

export { roomMenu };