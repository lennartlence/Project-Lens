import "./stylesheets/main.css";
import "./stylesheets/container.css";
import "./stylesheets/titlebar.css";
import "./stylesheets/player.css";
import "./stylesheets/tmdb.css";
import "./stylesheets/searchTab.css";
import "./stylesheets/playlist.css";
import "./stylesheets/roomMenu.css";
import "./stylesheets/navBar.css";
import "./stylesheets/playerSettings.css";

import "./titlebar.js";

import { remote } from "electron";

remote.powerSaveBlocker.start("prevent-display-sleep");

window.onload = () => {
  remote.getCurrentWindow().show();
};

