import { app, BrowserWindow } from "electron";
import env from "env";
import url from "url";
import path from "path";

if (env.name == 'production') {
  process.env['VLC_PLUGIN_PATH'] = process.cwd() + '/resources/app.asar.unpacked/node_modules/vlc-engine/plugins';
} else {
  process.env['VLC_PLUGIN_PATH'] = process.cwd() + '/node_modules/vlc-engine/plugins';
};

if (env.name !== "production") {
  const userDataPath = app.getPath("userData");
  app.setPath("userData", `${userDataPath} (${env.name})`);
}

app.on("ready", () => {
  const mainWindow = new BrowserWindow({
    backgroundColor: "#191919",
    width: 1120,
    height: 640,
    minWidth: 800,
    minHeight: 345,
    frame: false,
    show: false,
  });

  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, "app.html"),
      protocol: "file:",
      slashes: true
    })
  );

  if (env.name === "development") {
    mainWindow.openDevTools();
  }
});

app.on("window-all-closed", () => {
  app.quit();
});
