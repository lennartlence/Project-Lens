import { player } from "./player.js";
import { tmdb } from "./tmdb.js";
import { remote } from "electron";
import { titlebar } from "./titlebar";

class Main {
    constructor() {
        this.currentWindow = 'tmdb';
    };

    switchToPlayer() {
        this.currentWindow = 'player';
        
        tmdb.div.classList.add('hide');
        setTimeout(() => {
            tmdb.div.classList.add('displaynone');

            player.div.classList.remove('displaynone');
            setTimeout(() => {
                player.div.classList.remove('hide');
            }, 50);
        }, 250);
    };

    switchToTmdb() {
        this.currentWindow = 'tmdb';

        player.div.classList.add('hide');
        setTimeout(() => {
            player.div.classList.add('displaynone');

            tmdb.div.classList.remove('displaynone');
            setTimeout(() => {
                tmdb.div.classList.remove('hide');
                titlebar.div.style.opacity = 1;
            }, 50);
        }, 250);
    };

    minimize() {
        remote.getCurrentWindow().minimize();
    };

    fullscreen() {
        if (remote.getCurrentWindow().isFullScreen()) {
            remote.getCurrentWindow().setFullScreen(false);

            if (player.playerStatus == 2) {
                player.resizePlayer();
                player.div.classList.remove('fullscreen');
            };

            document.querySelector('#fullscreen-btn > svg:nth-child(1)').style.display = "block";
            document.querySelector('#fullscreen-btn > svg:nth-child(2)').style.display = "none";
        } else {
            remote.getCurrentWindow().setFullScreen(true);

            if (player.playerStatus == 2) {
                player.div.classList.add('fullscreen');
            };

            document.querySelector('#fullscreen-btn > svg:nth-child(1)').style.display = "none";
            document.querySelector('#fullscreen-btn > svg:nth-child(2)').style.display = "block";
        };
    };

    close() {
        remote.getCurrentWindow().close();
    };
};

const main = new Main();

export { main };