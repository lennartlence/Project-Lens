import { torrentSearch } from "./torrentSearch.js";
import { torrentEngine } from "./torrentEngine.js";
import { tmdb } from "./tmdb.js";

class SearchTab {
    constructor() {
        this.div = document.querySelector('#searchTab');
        this.ul = document.querySelector('#searchlist');
        this.inputDiv = document.querySelector('#searchTabInput');
        this.inputDivField = document.querySelector('#searchTabInputField');
        this.inputDivSpan = document.querySelector('#searchTabInputSpan');
        this.closeBtn = document.querySelector('#closeSearch');

        this.loader = document.querySelector('#searchTabLoader');

        this.activeTorrents = [];
        this.currentTorrent = null;

        this.queryState = true;
    };

    createActions() {
        this.inputDivField.addEventListener('focus', () => {
            this.inputDivSpan.style.opacity = 0;
        });
        this.inputDivField.addEventListener('blur', () => {
            if (this.inputDivField.value == "") {
                this.inputDivSpan.style.opacity = 1;
            };
        });
        this.inputDivField.addEventListener('keydown', (e) => {
            if (e.keyCode == 13) {
                this.searchTorrents(this.inputDivField.value);
            };
        });

        this.div.addEventListener('click', (e) => e.stopPropagation());

        this.closeBtn.addEventListener('click', (e) => this.toggleSearchTab());

        document.querySelector('.tmdb_background').addEventListener('click', (e) => this.toggleSearchTab());

        this.ul.addEventListener('click', (e) => {
            if (e.target.id !== 'searchlist') {
                const find = this.activeTorrents.find(i => { return i == e.target.id });

                if (find == undefined) {
                    this.currentTorrent = e.target.id;
                    this.activeTorrents.push(e.target.id);

                    document.getElementById(e.target.id).classList.add('active');
                    document.querySelectorAll('.searchListItem').forEach(e => e.classList.add('loading'));

                    torrentSearch.getMagnet(e.target.link).then(magnet => torrentEngine.newTorrent(magnet)).catch(err => {
                        document.querySelectorAll('.searchListItem').forEach(e => e.classList.remove('loading'));
                        document.querySelector(`#${searchTab.currentTorrent}`).classList.remove('active');

                        searchTab.activeTorrents.splice(searchTab.activeTorrents.findIndex(e => e == searchTab.currentTorrent), 1)
                    });
                };
            };
        });
    };

    toggleSearchTab() {
        this.div.classList.toggle('active');
        this.inputDiv.classList.toggle('active');

        document.querySelector('.tmdbCategory').classList.toggle('hide');
        document.querySelector('#tmdbDisplay').classList.toggle('hide');
        document.querySelector('.tmdb_background').classList.toggle('active');

        if (this.div.classList.contains('active') && !this.queryState) {
            this.loader.style.opacity = 1;
        } else {
            this.loader.style.opacity = 0;
        };
    };

    listHandler(id) {
        if (this.queryState) {
            tmdb.getItemInfo(id).then(item => {
                document.querySelector('#tmdbItemInfo > div > img').src = `${tmdb.image_base_url}w500${item.poster_path}`;
                document.querySelector('#tmdbItemInfo > img').src = `${tmdb.image_base_url}original${item.backdrop_path}`;
                document.querySelector('#tmdbItemInfo > div > div > div.title > div:nth-child(1) > span:nth-child(1)').innerHTML = item.title;
                document.querySelector('#tmdbItemInfo > div > div > div.title > div:nth-child(1) > span:nth-child(2)').innerHTML = item.release_date.split('-')[0]
                document.querySelector('#tmdbItemInfo > div > div > div.title > div.info_rate > span').innerHTML = item.vote_average;
                document.querySelector('#tmdbItemInfo > div > div > div.overview > span').innerHTML = item.overview;

                this.searchTorrents(`${item.original_title} ${item.release_date.split('-')[0]}`);
            });
        };
    };

    searchTorrents(val) {
        this.inputDivField.style.opacity = 0.5;
        setTimeout(() => {
            this.inputDivField.style.opacity = 1;
        }, 250);

        this.queryState = false;

        this.clearSearchResult();

        this.loader.style.opacity = 1;

        torrentSearch.search(val, 'Rutracker').then(result => {
            this.queryState = true;

            this.loader.style.opacity = 0;

            result.forEach(el => {
                this.drawSearchResult(el.id, el.title, el.size, el.seeds, el.link);
            })
        }).catch(err => {
            this.queryState = true;
            this.loader.style.opacity = 0;
        });
    };

    drawSearchResult(id, Name, Size, Seeds, Link) {
        let el = document.createElement('li');

        el.id = id;
        el.link = Link;

        el.classList.add('searchListItem');
        el.classList.add('hide');
        el.innerHTML =
            `<span class="searchListItemName">${Name}</span>
        <div class="searchListItemInfo">
            <div class="searchListItemSize">
                <span>${Size}</span>
            </div>
    
            <div class="searchListItemSeeds">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" style="fill: ${Seeds < 50 ? `${Seeds < 10 ? 'tomato' : 'steelblue'}` : 'seagreen'}">
                    <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z" />
                    <path d="M0 0h24v24H0z" fill="none" /></svg>
                <span>${Seeds}</span>
            </div>
        </div>`;

        this.ul.appendChild(el);

        setTimeout(() => {
            el.classList.remove('hide');

            const find = this.activeTorrents.find(i => { return i == el.id });

            if (find !== undefined) {
                el.classList.add('active');
            };
        }, 50);
    };

    clearSearchResult() {
        document.querySelectorAll('.searchListItem').forEach(e => e.classList.add('hide'));

        setTimeout(() => {
            this.ul.innerHTML = '';
        }, 250);
    };
};

const searchTab = new SearchTab();

searchTab.createActions();

export { searchTab };