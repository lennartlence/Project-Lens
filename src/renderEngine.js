import vlcEngine from "vlc-engine";
import { player } from "./player.js";
import { playlist } from "./playlist";
import { torrentEngine } from "./torrentEngine.js";

import { bind } from "./render";

class RenderEngine {
    constructor(render, file) {
        this.engine = vlcEngine.createPlayer();

        this.render = render;
        this.file = file

        this.seekState = false;
        this.buffState = 'Checking';
        this.buffTime = 0;
        this.lastTime = 0;
    };

    startEngine() {
        bind(this.render, this.engine);

        this.engine.play(this.file);

        if (this.file.includes('http')) {
            const file = torrentEngine.torrentEngine.get(playlist.listCurrent.torrentId).files[playlist.listCurrent.fileIndex];
            const filePath = require('downloads-folder')() + "/Project Lens Downloads/" + file.path.replace(/\\/g, '/');

            if (!require('fs').existsSync(filePath)) {
                processing();
            } else {
                if (require('fs').statSync(filePath).size < file.length) {
                    processing();
                };
            };

            function processing() {
                document.querySelector('#playerLoader span').innerHTML = `Processing ${require('prettysize')(file.length)}`;
                document.querySelector('#playerLoader').classList.add('active');
                document.querySelector('#playerLoader span').classList.add('active');
            };
        };

        this.engine.onPlaying = () => {
            if (this.engine.time < 100) {
                const flac = playlist.listCurrent.filePath.endsWith('.flac');
                const mp3 = playlist.listCurrent.filePath.endsWith('.mp3');
                const wav = playlist.listCurrent.filePath.endsWith('.wav');

                player.itemReady();



                this.engine.volume = player.volume;

                // this.lagCheckingHandler();

                document.querySelector('#timerDuration').innerHTML = player.setFixedTime(this.engine.length);

                if (playlist.listCurrent.parsedInfo.episode !== undefined) {
                    console.log(`Playing: ${playlist.listCurrent.title} Season: ${playlist.listCurrent.parsedInfo.season} Episode: ${playlist.listCurrent.parsedInfo.episode}`);
                } else {
                    console.log(`Playing: ${playlist.listCurrent.title}`);
                };

                if ((flac || mp3) || wav) {
                    this.audioReady();
                };
            };

            this.lastTime = this.engine.time - 500;
        };

        this.engine.onPaused = () => {
            if (!this.seekState & this.buffState == 'Checking') {

            };
        };

        this.engine.onEndReached = () => {
            player.resetPlayer();
        };

        this.engine.onLogMessage = (pe, tu, ch) => {
            if (tu.includes('Stream buffering done')) {
                if (this.seekState) {
                    this.seekState = false;
                    this.bufferingDone();
                };

                if (this.buffState == 'Done') {
                    this.bufferingDone();
                };
            };
        };
    };

    videoReady() {
        player.resizePlayer();
        player.render.style.opacity = 1;

        this.engine.subtitles.track = 0;

        for (let index = 0; index < this.engine.audio.count; index++) {
            let el = document.createElement('div');

            el.id = index;
            el.classList.add('listItem');
            el.innerHTML = `<span>${this.engine.audio[index]}</span>`;

            document.querySelector('#audioList').appendChild(el);
        };

        for (let index = 0; index < this.engine.subtitles.count; index++) {
            let el = document.createElement('div');

            el.id = index;
            el.classList.add('listItem');
            el.innerHTML = `<span>${this.engine.subtitles[index]}</span>`;

            document.querySelector('#subtitlesList').appendChild(el);
        };

        document.querySelector('#currentSubtitle span').innerHTML = `Disable`;
        document.querySelector('#currentAudio span').innerHTML = `${this.engine.audio[this.engine.audio.track]}`;
    };

    audioReady() {

    };

    seekto(percent) {
        if (!this.seekState) {
            this.lastTime = this.engine.time - 500;

            this.buffState = 'Checking';
            this.engine.mute = false;

            this.seekState = true;

            this.engine.pause();
            this.engine.position = percent;
        };
    };

    lagCheckingHandler() {
        console.log(this.lastTime, this.engine.time)
        if (this.lastTime == this.engine.time & this.lastTime > 0) {
            if (this.buffState == 'Checking') {
                if (this.lagCheckingState()) {
                    this.buffering();
                };
            } else if (this.buffState == 'Processing') {
                this.engine.time = this.engine.time - 500;
            };
        } else {
            this.lastTime = this.engine.time;
        };

        setTimeout(() => {
            if (this.engineCheck()) {
                this.lagCheckingHandler();
            };
        }, 1000);
    };

    engineCheck() {
        if (this.engine !== undefined) {
            if (this.engine.videoFrame !== undefined) {
                return true;
            } else {
                return false;
            };
        } else {
            return false;
        };
    };

    lagCheckingState() {
        if (!this.seekState && player.playState == 'play') {
            return true;
        };
    };

    buffering() {
        console.log('Buffering...');

        document.querySelector('#playerLoader').classList.add('active');

        this.engine.time = this.engine.time - 500;

        this.buffState = 'Processing';
        this.engine.mute = true;

        this.buffTime = this.engine.time;

        this.buffTimeCheck();
    };

    bufferingDone() {
        console.log('Buffering Done');

        document.querySelector('#playerLoader').classList.remove('active');

        this.engine.pause();

        this.lastTime = this.engine.time - 500;

        this.buffState = 'Checking';
        this.engine.mute = false;

        if (player.playState == 'play') {
            this.engine.play();
        };
    };

    buffTimeCheck() {
        if (this.engine.time > this.buffTime + 1000) {
            this.buffState = 'Done';
            this.engine.time = this.buffTime + 500;
        } else {
            setTimeout(() => {
                if (this.buffState == 'Processing') {
                    this.buffTimeCheck();
                };
            }, 200);
        };
    };
};

export { RenderEngine };