import { RenderEngine } from "./renderEngine";
import { remote } from "electron";
import { playlist } from "./playlist";
import { titlebar } from "./titlebar";
import { main } from "./main";

class Player {
    constructor() {
        this.div = document.querySelector('#player');
        this.renderContainer = document.querySelector('#renderContainer');
        this.render = document.querySelector('#render');
        this.loader = document.querySelector('#playerLoader');
        this.timeline = document.querySelector('#timeline');

        this.cropVideo = false;
        this.volume = 100;
        this.renderEngine = null;

        this.playerStatus = 0;
        this.playState = null;

        this.timeoutHide = 0;
    };

    createActions() {
        window.addEventListener('keydown', (e) => {
            if (document.activeElement.id == '' && this.playerStatus == 2 && main.currentWindow == 'player') {
                if (e.keyCode == 82) {
                    this.resizePlayer();
                };
            };
        });

        this.renderContainer.addEventListener('click', () => {
            if (this.playerStatus == 2) {
                this.togglePlay();
            };
        });

        this.timeline.addEventListener('click', (e) => {
            e.stopPropagation();

            let percent = event.offsetX / document.querySelector('#timeline').offsetWidth;

            this.seekTo(percent);
        });

        this.timeline.addEventListener('mouseover', () => {
            document.querySelector('#timerCurrent').classList.add('timerSpanGray');
        });

        this.timeline.addEventListener('mouseout', () => {
            document.querySelector('#timerCurrent').classList.remove('timerSpanGray');
            document.querySelector('#timerCurrent').innerHTML = this.setFixedTime(this.renderEngine.engine.time);
        });

        this.timeline.addEventListener('mousemove', (e) => {
            if (this.playerStatus == 2) {
                let percent = e.offsetX / document.querySelector('#timeline').offsetWidth;

                document.querySelector('#timerCurrent').innerHTML = this.setFixedTime(this.renderEngine.engine.length * percent);
            };
        });

        window.addEventListener('mousemove', () => {
            titlebar.div.style.opacity = 1;
            document.getElementById('player').style.cursor = 'default';

            if (this.playerStatus == 2 && main.currentWindow == 'player') {
                document.querySelector('#navBar').classList.add('active');

                clearTimeout(this.timeoutHide);

                this.timeoutHide = setTimeout(() => {
                    if (this.playerStatus == 2 && main.currentWindow == 'player') {
                        titlebar.div.style.opacity = 0;
                        document.querySelector('#navBar').classList.remove('active');
                        document.getElementById('player').style.cursor = 'none';
                    };
                }, 3000);

            };
        });
    };

    play() {
        this.playState = 'play';

        this.renderEngine.engine.play();
    };

    pause() {
        this.playState = 'pause';

        this.renderEngine.engine.pause();
    };

    togglePlay() {
        switch (this.playState) {
            case 'play':
                this.pause();
                break;
            case 'pause':
                this.play();
                break;
            default:
                break;
        };
    };

    resetPlayer() {
        if (playlist.listCurrent !== null) {
            playlist.listCurrentDiv.classList.remove('active');
            playlist.listCurrentDiv.classList.remove('tryActive');
        };

        playlist.listCurrent = null;

        this.playerStatus = 0;

        document.querySelector('#playerLoader').classList.remove('active');
        document.querySelector('#navBar').classList.remove('active');

        document.querySelector('#currentSubtitle span').innerHTML = 'Disable';
        document.querySelector('#currentAudio span').innerHTML = 'Disable';

        document.querySelector('#subtitlesList').innerHTML = '';
        document.querySelector('#audioList').innerHTML = '';

        titlebar.hidden = false;
        titlebar.div.classList.remove('hideTitlebar');

        player.div.classList.remove('fullscreen');

        this.render.style.opacity = 0;

        setTimeout(() => {
            if (this.renderEngine !== null) {
                this.renderEngine.engine.close();
                this.renderEngine = null;
            };
        }, 250);
    };

    playItem(item) {
        this.playerStatus = 1;

        setTimeout(() => {
            switch (item.playerType) {
                case 'Torrent':
                    this.renderEngine = new RenderEngine(this.render, `http://localhost:${item.sessionPort}/${item.fileIndex}`);

                    this.renderEngine.startEngine();
                    break;

                default:
                    break;
            };
        }, 250);
    };

    itemReady() {
        this.playState = 'play';
        this.playerStatus = 2;

        playlist.listCurrentDiv.classList.remove('tryActive');
        playlist.listCurrentDiv.classList.add('active');

        document.querySelector('#playerLoader').classList.remove('active');
        document.querySelector('#playerLoader span').classList.remove('active');

        document.querySelector('#navBar').classList.add('active');

        this.synchTimeline();

        if (remote.getCurrentWindow().isFullScreen()) {
            player.div.classList.add('fullscreen');
        };

        titlebar.hidden = true;

        if (playlist.div.classList.contains('active')) {
            playlist.div.classList.toggle('active');
            playlist.inputDiv.classList.toggle('active');
            titlebar.playlistBtn.classList.toggle('active');
        };

        setTimeout(() => {
            if (main.currentWindow == 'player') {
                document.querySelector('#titlebar').classList.add('hideTitlebar');
            };
        }, 250);
    };

    cropVideo() {
        if (this.cropVideo) {
            this.cropVideo = false
        } else {
            this.cropVideo = true;
        };

        switch (playlist.listCurrent.playerType) {
            case 'Torrent':
                this.render.classList.toggle('cropVideo');
                break;

            default:
                break;
        };
    };

    synchTimeline() {
        if (this.renderEngine !== null) {
            if (this.renderEngine.buffState == 'Checking') {
                document.querySelector('#timewatch').style.width = this.renderEngine.engine.position * 100 + '%';

                if (!document.querySelector('#timerCurrent').classList.contains('timerSpanGray')) {
                    document.querySelector('#timerCurrent').innerHTML = this.setFixedTime(this.renderEngine.engine.time);
                };
            };
        };

        setTimeout(() => {
            if (this.playerStatus == 2) {
                this.synchTimeline();
            };
        }, 100);
    };

    setFixedTime(ms) {
        let time = require('pretty-ms')(Math.round(ms), { keepDecimalsOnWholeSeconds: true, secDecimalDigits: 0 });

        if (time.includes('ms')) {
            return '0s';
        } else {
            return time;
        };
    };

    resizePlayer() {
        if (remote.getCurrentWindow().isMaximized() == false && remote.getCurrentWindow().isFullScreen() == false) {
            const currentSize = remote.getCurrentWindow().getSize();

            let videoHeight;
            let videoWidth

            switch (playlist.listCurrent.playerType) {
                case 'Torrent':
                    videoHeight = document.querySelector('canvas').height;
                    videoWidth = document.querySelector('canvas').width;

                    this.render.classList.add('cropVideo');
                    break;

                default:
                    break;
            };

            remote.getCurrentWindow().setMinimumSize(640, Math.round(videoHeight / (videoWidth / 640)));

            remote.getCurrentWindow().setContentSize(currentSize[0], Math.round(videoHeight / (videoWidth / currentSize[0])));

            setTimeout(() => {
                window.addEventListener('resize', () => {
                    if (this.cropVideo == false) {
                        this.render.classList.remove('cropVideo');
                    };
                }, { once: true });
            }, 500);
        };
    };

    seekTo(percent) {
        document.querySelector('#playerLoader').classList.add('active');

        switch (playlist.listCurrent.playerType) {
            case 'Torrent':
                this.renderEngine.seekto(percent);
                break;
            case 'Youtube':

                break;
            case 'DirectVideo':

                break;
            default:
                break;
        };
    };
};

const player = new Player();

player.createActions();

export { player };
