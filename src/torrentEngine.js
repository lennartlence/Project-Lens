import webTorrent from 'webtorrent';
import { playlist } from "./playlist.js";
import { searchTab } from "./searchTab";

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
};

class TorrentEngine {
    constructor() {
        this.torrentEngine = new webTorrent();
    };

    newTorrent(torrentId) {
        this.torrentEngine.add(torrentId, { path: require('downloads-folder')() + "/Project Lens Downloads" });

        const tSession = this.torrentEngine.get(torrentId);
        const tSessionPort = Math.floor(1000 + Math.random() * 9000);

        setTimeout(() => {
            if (tSession.metadata == null) {
                console.log('Torrent Fieled');

                tSession.destroy();

                document.querySelectorAll('.searchListItem').forEach(e => e.classList.remove('loading'));
                document.querySelector(`#${searchTab.currentTorrent}`).classList.remove('active');

                searchTab.activeTorrents.splice(searchTab.activeTorrents.findIndex(e => e == searchTab.currentTorrent), 1)
            };
        }, 5000);

        let items = [];

        tSession.on('metadata', () => {
            console.log('Received Metadata');

            // document.querySelector('#tmdbSpan').innerHTML = 'Received Metadata...';
            // document.querySelector('#tmdbSpan').style.opacity = 1;

            setTimeout(() => {
                if (tSession.files.find(file => {
                    return require('fs').existsSync(require('downloads-folder')() + "/Project Lens Downloads/" + file.path.replace(/\\/g, '/'));
                }) !== undefined) {
                    console.log('Verifying Files...');

                    document.querySelector('#tmdbSpan').style.opacity = 0;
                    setTimeout(() => {
                        document.querySelector('#tmdbSpan').innerHTML = 'Verifying Files...';
                        document.querySelector('#tmdbSpan').style.opacity = 1;
                    }, 200);
                };
            }, 1000);

            console.log(tSession)

            tSession.deselect(0, tSession.pieces.length - 1, false);

            const filteredArr = tSession.files.filter(index => {
                if (index.path.endsWith('.mkv')) {
                    return index;
                } else if (index.path.endsWith('.avi')) {
                    return index;
                } else if (index.path.endsWith('.mp4')) {
                    return index;
                } else if (index.path.endsWith('.flac')) {
                    return index;
                } else if (index.path.endsWith('.mp3')) {
                    return index;
                } else if (index.path.endsWith('.wav')) {
                    return index;
                } else if (index.path.endsWith('.m4v')) {
                    return index;
                };
            });

            filteredArr.forEach(element => {
                let i = tSession.files.findIndex(index => { return index.path == element.path });
                let p = require("torrent-name-parser")(tSession.files[i].name);
                let t = toTitleCase(p.title);

                if (t.length < 2) {
                    t = 'Unknown';
                };

                let item = {
                    playerType: 'Torrent',
                    torrentId: torrentId,
                    sessionPort: tSessionPort,
                    fileIndex: i,
                    title: t,
                    parsedInfo: p,
                    filePath: element.path
                };

                playlist.list.push(item);

                items.push(item);
            });

            if (items == undefined) {
                items = [];
            };

            playlist.drawList();
        });

        tSession.on('ready', () => {
            console.log('Torrent Ready');

            document.querySelector('#tmdbSpan').style.opacity = 0;
            setTimeout(() => {
                document.querySelector('#tmdbSpan').innerHTML = 'Player is ready!';
                document.querySelector('#tmdbSpan').style.opacity = 1;
                setTimeout(() => {
                    document.querySelector('#tmdbSpan').style.opacity = 0;
                    document.querySelectorAll('.searchListItem').forEach(e => e.classList.remove('loading'));
                }, 2000);
            }, 200);

            tSession.createServer().listen(tSessionPort);

            setTimeout(() => {
                playlist.ready();
            }, 100);
        });
    };
};

const torrentEngine = new TorrentEngine();

export { torrentEngine };
