import { player } from "./player";

class Playlist {
    constructor() {
        this.list = [];
        this.listCurrent = null;
        this.listCurrentDiv = null;

        this.ul = document.querySelector('#playlist');
        this.div = document.querySelector('#playlistMenu');
        this.inputDiv = document.querySelector('#playlistInput');
        this.inputDivField = document.querySelector('#playlistInputField');
        this.inputDivSpan = document.querySelector('#playlistInputSpan');
    };

    createActions() {
        this.inputDivField.addEventListener('focus', () => {
            this.inputDivSpan.style.opacity = 0;
        });
        this.inputDivField.addEventListener('blur', () => {
            if (this.inputDivField.value == "") {
                this.inputDivSpan.style.opacity = 1;
            };
        });
        this.inputDivField.addEventListener('keydown', (e) => {
            if (e.keyCode == 13) {
                console.log(this.list);
            };
        });
        this.inputDivField.addEventListener('contextmenu', (e) => {
            e.preventDefault();
            document.execCommand('paste');
        });

        this.ul.addEventListener('click', (e) => {
            if (e.target.id !== 'playlist') {
                player.resetPlayer();

                this.listCurrent = this.list[e.target.id.split('_')[1]];
                this.listCurrentDiv = document.getElementById(e.target.id);
                this.listCurrentDiv.classList.add('tryActive');

                player.playItem(this.listCurrent);
            };
        });
    };

    drawList() {
        this.list.forEach((file, index) => {
            if (document.getElementById('playlistItem_' + index) == null) {
                switch (file.playerType) {
                    case 'Torrent':
                        this.addItem(index, `${file.title} ${!!file.parsedInfo.year ? '| ' + file.parsedInfo.year : ''}`, `${file.parsedInfo.episode !== undefined ? `Season: ${file.parsedInfo.season} Episode: ${file.parsedInfo.episode}` : ''}`);
                        break;
                    case 'Youtube':
                        this.addItem(index, file.title);
                        break;
                    case 'DirectVideo':
                        this.addItem(index, file.title);
                        break;
                    default:
                        break;
                };
            };
        });
    };

    addItem(id, name, name2) {
        let el = document.createElement('li');

        el.id = 'playlistItem_' + id;
        el.classList.add('playlistListItem');
        el.innerHTML = `<span class="playlistListItemName">${name}</span> ${!!name2 ? `<span class="playlistListItemName2">${name2}</span>` : ''}`;

        this.ul.appendChild(el);

        setTimeout(() => {
            el.classList.add('metadata');
        }, 50);
    };

    ready() {
        document.querySelectorAll('.metadata').forEach(e => e.classList.add('ready'));
    };
};

const playlist = new Playlist();

playlist.createActions();

export { playlist };