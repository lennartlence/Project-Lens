import { searchTab } from "./searchTab";

class Tmdb {
    constructor() {
        this.div = document.querySelector('#tmdb');
        this.itemDisplayer = document.querySelector('#tmdbDisplay');

        this.items = document.querySelector('.tmdbItem');

        this.apiKey = 'aa21cca3c7d374d8c82585f6705c6137';
        this.base_url = 'https://api.themoviedb.org/3/';
        this.image_base_url = 'http://image.tmdb.org/t/p/';

        this.lang = 'en-US';

        this.release_year;
        this.category;
        this.gener;

        this.pageCount = 0;
        this.pageToLoad = 3;

        this.loadingNewItems = false;

        this.currentCategory = 'movie';
    };

    createActions() {
        this.itemDisplayer.addEventListener('scroll', (e) => {
            if (e.target.scrollHeight - e.target.scrollTop < e.target.clientHeight + 300) {
                if (!this.loadingNewItems) {
                    this.getPages();
                };
            };
        });

        this.itemDisplayer.addEventListener('click', (e) => {
            if (e.target.id !== 'tmdbDisplay') {
                if (!searchTab.div.classList.contains('active')) {
                    searchTab.toggleSearchTab();
                };

                searchTab.listHandler(e.target.tmdbInfo.id);
            };
        });
    };

    tmdbSearch(query) {
        return new Promise(() => {
            this.loadingNewItems = true;
            this.pageCount = 1;
            this.itemDisplayer.innerHTML = '';

            const postData = require('querystring').stringify({
                api_key: this.apiKey,
                language: this.lang,
                page: this.pageCount,
                query: query,
            })

            fetch(`${this.base_url}search/multi?${postData}`).then(result => {
                result.json().then(result => {
                    const items = document.querySelectorAll('#tmdbDisplay div');
                    let newItems = result.results;

                    let processed = 0;

                    if (items.length == 0) {
                        this.drawItems(newItems);
                    } else {
                        items.forEach(item => {
                            let index = newItems.findIndex(e => { return e.id == item.id });

                            if (index == 0) {
                                newItems.splice(index, 1);
                            } else {
                                item.remove();
                            };

                            ++processed

                            if (processed == items.length && newItems.length > 1) {
                                this.drawItems(newItems);
                            };
                        });
                    };
                });
            });
        });
    };

    getData() {
        return new Promise(resolve => {
            switch (this.currentCategory) {
                case 'movie':
                    this.discoverMovie().then(arr => {
                        this.drawItems(arr).then(() => resolve());
                    });
                    break;
                case 'tv':
                    this.discoverTv().then(arr => {
                        this.drawItems(arr).then(() => resolve());
                    });
                    break;

                default:
                    break;
            };
        });
    };

    discoverMovie() {
        return new Promise((resolve) => {
            this.loadingNewItems = true;
            ++this.pageCount;

            const postData = require('querystring').stringify({
                api_key: this.apiKey,
                language: this.lang,
                page: this.pageCount,
                primary_release_year: this.release_year,
                with_genres: this.gener
            });

            fetch(`${this.base_url}discover/${this.currentCategory}?${postData}`).then(result => {
                result.json().then(result => {
                    resolve(result.results);
                });
            });
        });
    };

    discoverTv() {
        return new Promise((resolve) => {
            this.loadingNewItems = true;
            ++this.pageCount;

            const postData = require('querystring').stringify({
                api_key: this.apiKey,
                language: this.lang,
                page: this.pageCount,
                first_air_date_year: this.release_year,
                with_genres: this.gener
            });

            fetch(`${this.base_url}discover/tv?${postData}`).then(result => {
                result.json().then(result => {
                    resolve(result.results);
                });
            });
        });
    };

    getItemInfo(id) {
        return new Promise(resolve => {
            const postData = require('querystring').stringify({
                api_key: this.apiKey,
                language: this.lang,
            });

            fetch(`${this.base_url}${this.currentCategory}/${id}?${postData}`).then(result => {
                result.json().then(result => {
                    resolve(result);
                });
            });
        });
    };

    drawItems(items) {
        return new Promise(resolve => {
            let count = 0;
            let itemsLength = items.length;

            for (let i = 0; i < items.length; i++) {
                const e = items[i];

                const itemInfo = {
                    id: e.id,
                    title: e.title,
                    original_title: e.original_title,
                    vote: e.vote_average,
                    thumbnail: `${this.image_base_url}w500${e.poster_path}`,
                    backdrop: `${this.image_base_url}original${e.backdrop_path}`
                };

                if (!itemInfo.thumbnail.includes('null') && !itemInfo.thumbnail.includes('undefined') && /[A-z]/g.test(itemInfo.original_title)) {
                    const el = document.createElement('div');

                    el.tmdbInfo = itemInfo;

                    el.classList.add('tmdbItem');
                    el.innerHTML = `<img src="${itemInfo.thumbnail}" style="opacity: 0">`;

                    this.itemDisplayer.appendChild(el);

                    let imgs = el.childNodes[0];

                    imgs.onload = () => {
                        imgs.style.opacity = 1;

                        ++count;

                        if (count == itemsLength) {
                            this.loadingNewItems = false;
                            resolve();
                        };
                    };
                } else {
                    itemsLength = itemsLength - 1;
                };
            };
        });
    };

    getPages(category, count) {
        (async () => {
            try {
                if (category !== undefined) {
                    this.currentCategory = category;
                    this.pageCount = 0;
                    this.itemDisplayer.innerHTML = '';
                };

                let c;

                if (count !== undefined) {
                    c = count
                } else {
                    c = this.pageToLoad;
                };

                for (let i = 0; i < c; i++) {
                    await tmdb.getData();
                };
            } catch (error) {
                console.log(error)
            };
        })();
    };
};

const tmdb = new Tmdb();

tmdb.createActions();

tmdb.getPages('movie');

export { tmdb };


