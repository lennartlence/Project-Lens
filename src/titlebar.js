import { main } from "./main.js";
import { searchTab } from "./searchTab.js";
import { playlist } from "./playlist.js";
import { roomMenu } from "./roomMenu.js"
import { playerSettings } from "./playerSettings.js";
import { tmdb } from "./tmdb.js";

class Titlebar {
    constructor() {
        this.div = document.querySelector('#titlebar');

        this.tmdbBtns = document.querySelectorAll('.tmdb-btn');
        this.playerBtns = document.querySelectorAll('.player-btn');

        this.homeBtn = document.querySelector('#home-btn');
        this.playerBtn = document.querySelector('#player-btn');
        this.backBtn = document.querySelector('#back-btn');
        this.searchBtn = document.querySelector('#search-btn');
        this.playlistBtn = document.querySelector('#playlist-btn');

        this.settingsBtn = document.querySelector('#settings-btn');
        this.minimizeBtn = document.querySelector('#minimize-btn');
        this.fullscreenBtn = document.querySelector('#fullscreen-btn');
        this.closeBtn = document.querySelector('#close-btn');

        this.searchinput = document.querySelector('#search-btn > input');

        this.hidden = false;
    };

    createActions() {
        this.div.addEventListener('mouseover', () => {
            this.div.classList.remove('hideTitlebar');
        });

        this.div.addEventListener('mouseout', () => {
            if (this.hidden && playlist.div.classList.contains('active') == false && main.currentWindow == 'player') {
                this.div.classList.add('hideTitlebar');
            };
        });

        this.homeBtn.addEventListener('mousedown', () => {
            tmdb.itemDisplayer.classList.remove('tmdbHidePage');

            document.querySelector('.tmdbCategory').classList.remove('hide');
        });

        this.playerBtn.addEventListener('mousedown', () => {
            main.switchToPlayer();
            this.switchToPlayer();
        });

        document.querySelector('#search-btn').addEventListener('mouseover', () => {
            this.searchBtn.classList.add('hover');
            this.searchinput.classList.add('hover')
        })
        document.querySelector('#search-btn').addEventListener('mouseout', () => {
            this.searchBtn.classList.remove('hover');
            this.searchBtn.classList.remove('active');

            this.searchinput.classList.remove('hover');
            this.searchinput.blur();
        })

        this.searchinput.addEventListener('focus', () => { this.searchBtn.classList.add('active'); });

        this.searchinput.addEventListener('keydown', (e) => {
            // if (e.keyCode == 13) {
            setTimeout(() => {
                if (this.searchinput.value.length > 0) {
                    document.querySelector('#tmdbMovie').classList.remove('active');
                    document.querySelector('#tmdbTv').classList.remove('active');

                    tmdb.tmdbSearch(this.searchinput.value);
                } else {
                    document.querySelector('#tmdbMovie').classList.add('active');
                    document.querySelector('#tmdbTv').classList.remove('active');

                    tmdb.getPages('movie');
                };
            }, 100);
            // };
        });

        document.querySelector('#tmdbMovie').addEventListener('click', () => {
            document.querySelector('#tmdbMovie').classList.add('active');
            document.querySelector('#tmdbTv').classList.remove('active');

            tmdb.getPages('movie');
        });
        document.querySelector('#tmdbTv').addEventListener('click', () => {
            document.querySelector('#tmdbMovie').classList.remove('active');
            document.querySelector('#tmdbTv').classList.add('active');

            tmdb.getPages('tv');
        });

        this.playlistBtn.addEventListener('mousedown', () => {
            this.playlistBtn.classList.toggle('active');
            playlist.div.classList.toggle('active');
            playlist.inputDiv.classList.toggle('active');
        });

        this.backBtn.addEventListener('mousedown', () => {
            main.switchToTmdb();
            this.switchToTmdb();
        });

        this.settingsBtn.addEventListener('click', () => {
            this.settingsBtn.classList.toggle('active');
            document.querySelector('#playerSettings').classList.toggle('active');
        });

        this.minimizeBtn.addEventListener('click', () => {
            main.minimize();
        });
        this.fullscreenBtn.addEventListener('click', () => {
            main.fullscreen();
        });
        this.closeBtn.addEventListener('click', () => {
            main.close();
        });
    };

    switchToPlayer() {
        for (const e of this.tmdbBtns) {
            e.classList.add('hide');
        };

        setTimeout(() => {
            this.currentWindow = 'player';

            for (const e of this.tmdbBtns) {
                e.classList.add('displaynone');
            };

            for (const e of this.playerBtns) {
                e.classList.remove('displaynone');
            };

            setTimeout(() => {
                for (const e of this.playerBtns) {
                    e.classList.remove('hide');
                };
            }, 50);
        }, 250);
    };

    switchToTmdb() {
        for (const e of this.playerBtns) {
            e.classList.add('hide');
        };

        setTimeout(() => {
            this.currentWindow = 'tmdb';

            for (const e of this.playerBtns) {
                e.classList.add('displaynone');
            };

            for (const e of this.tmdbBtns) {
                e.classList.remove('displaynone');
            };
            setTimeout(() => {
                for (const e of this.tmdbBtns) {
                    e.classList.remove('hide');
                };
            }, 50);
        }, 250);
    };
};

const titlebar = new Titlebar();

titlebar.createActions();

export { titlebar };